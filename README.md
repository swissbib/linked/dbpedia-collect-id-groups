## Dbpedia Global ID Collector

A flink job to combine the dbpedia global id tsv file into a simple JSON structure
for indexing into elasticsearch.

### Deployment

1. In `sb-ust1:/swissbib_index/apps` do `git clone https://gitlab.com/swissbib/linked/dbpedia-collect-id-groups.git`
2. `cd dbpedia-collect-id-groups`
3. `./deploy.sh`

### Run

In `/swissbib_index/apps/flink`

`./bin/flink run /swissbib_index/apps/dbpedia-collect-id-groups-assembly-1.0.0.jar --conf /swissbib_index/nas/configurations/dbpedia/uris/sort/app.properties &`