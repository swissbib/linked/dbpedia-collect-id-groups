#!/usr/bin/env bash

sbt clean assembly
rm /swissbib_index/apps/dbpedia-collect-id-groups-assembly-*.jar
cp target/scala-2.12/dbpedia-collect-id-groups-assembly-*.jar /swissbib_index/apps