/*
 * dbpedia global id cluster collector
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import java.util.Properties

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.streaming.api.scala._
import org.apache.logging.log4j.scala.Logging

import scala.io.Source
import scala.util.matching.Regex

object Job extends Logging {
  val patternLA: Regex = "@[^\"]+$".r
  val patternId: Regex = "^<http://viaf.org/viaf/(\\d+?)>".r

  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    val params = ParameterTool.fromArgs(args)
    val conf = new Properties
    val path = params.get("config")
    if (path == null) {
      logger.error("Pass a configuration file as path to --config!")
      System.exit(1)
    }
    conf.load(Source.fromFile(path).bufferedReader())
    logger.info("Loading config from " + params.get("config", "app.properties"))
    env.setParallelism(conf.getProperty("flink.parallelism", "3").toInt)
    env.getConfig.setGlobalJobParameters(params)

    val stream = env.readTextFile(conf.getProperty("flink.input.file"))

    stream
        .map(t => t.split('\t'))
        .map(t => new DbpediaIdTriple(t.apply(0), t.apply(1), t.apply(2)))
        .map(t => (t.globalUri(), t.uri))
        .groupBy(0)
        .reduce((e, a) => (a._1, a._2 + "\",\"" + e._2))
        .map(t => (t._1, "{\"dbpedia\":[\"" + t._2 + "\"]}"))
        .map(t => new SbMetadataModel().setData(t._2).setMessageKey(t._1))
        .output(new KafkaSink(conf))
    env.execute(conf.getProperty("flink.job.name"))
  }
}
